@echo off
set "VIRTUAL_ENV=C:\programs\jenkins\workspace\s16_sdp_noname\code\civenv"

if defined _OLD_VIRTUAL_PROMPT (
    set "PROMPT=%_OLD_VIRTUAL_PROMPT%"
) else (
    if not defined PROMPT (
        set "PROMPT=$P$G"
    )
    set "_OLD_VIRTUAL_PROMPT=%PROMPT%"
)
set "PROMPT=(civenv) %PROMPT%"

REM Don't use () to avoid problems with them in %PATH%
if defined _OLD_VIRTUAL_PYTHONHOME goto ENDIFVHOME
    set "_OLD_VIRTUAL_PYTHONHOME=%PYTHONHOME%"
:ENDIFVHOME

set PYTHONHOME=

REM if defined _OLD_VIRTUAL_PATH (
if not defined _OLD_VIRTUAL_PATH goto ENDIFVPATH1
    set "PATH=%_OLD_VIRTUAL_PATH%"
:ENDIFVPATH1
REM ) else (
if defined _OLD_VIRTUAL_PATH goto ENDIFVPATH2
    set "_OLD_VIRTUAL_PATH=%PATH%"
:ENDIFVPATH2

set "PATH=%VIRTUAL_ENV%\Scripts;%PATH%"
set "NEO4J_REST_URL=http://neo4j:agilecs@localhost:7474/db/data/"

cmd /K "C:\programs\neo4j\Neo4jCE2.3.2\neo4j-community-2.3.2\bin\Neo4j.bat" console

rem So apparently abusing ping is the most reliable way to sleep for 5 seconds http://stackoverflow.com/questions/1672338/how-to-sleep-for-5-seconds-in-windowss-command-prompt-or-dos
ping 127.0.0.1 -n 6 > nul

pip install -r requirements.txt
coverage run --source='.' --omit=analytics/tests/*,*venv/*,analytics/admin.py,analytics/apps.py,deep_email/wsgi.py manage.py test
set "ERR=%ERRORLEVEL%"
coverage html
exit %ERR%
