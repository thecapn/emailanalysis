from datetime import datetime, time

from hashlib import md5

import pytz
from neomodel import DateTimeProperty, StringProperty, RelationshipTo, StructuredNode, db

from analytics.cypher import get_keyword_subquery, get_email_address_subquery, get_start_date_subquery, \
    get_end_date_subquery, combine_subqueries
from analytics.models.EmailAddress import EmailAddress


class Email(StructuredNode):
    NO_SUBJECT_PLACEHOLDER = "(No Subject)"
    date = DateTimeProperty(required=True)
    subject_in_db = StringProperty(required=True)
    body = StringProperty(required=True)
    body_hash = StringProperty(required=True)

    in_folder = RelationshipTo('.Folder.Folder', 'IN')
    to = RelationshipTo('.EmailAddress.EmailAddress', 'TO')
    cc = RelationshipTo('.EmailAddress.EmailAddress', 'CC')
    bcc = RelationshipTo('.EmailAddress.EmailAddress', 'BCC')
    from_address = RelationshipTo('.EmailAddress.EmailAddress', 'FROM')

    @staticmethod
    def get_body_hash(body):
        return md5(body.encode('utf-8')).hexdigest()

    @property
    def date_cst(self):
        return self.date.astimezone(pytz.timezone("America/Denver"))

    @property
    def subject(self):
        return self.subject_in_db or self.NO_SUBJECT_PLACEHOLDER

    @subject.setter
    def subject(self, value):
        self.subject_in_db = value

    @staticmethod
    def search(keywords, start_date, end_date, people):
        query_string = combine_subqueries(Email, get_keyword_subquery(keywords), get_email_address_subquery(people),
                                          get_start_date_subquery(start_date), get_end_date_subquery(end_date))

        results, meta = db.cypher_query(query_string)
        emails = [Email.inflate(row[0]) for row in results]
        return emails

    def pre_save(self):
        self.body_hash = Email.get_body_hash(self.body)

    def fill_data_from_string(self, folder_containing, lines):
        self.date = get_date(lines)
        self.subject = get_subject(lines)
        self.body = get_body(lines)
        self.save()
        self.in_folder.connect(folder_containing)

        for address in get_to(lines):
            if not address:
                continue
            email_address = EmailAddress.get_or_create({'address': address})[0]
            self.to.connect(email_address)

        for address in get_cc(lines):
            if not address:
                continue
            email_address = EmailAddress.get_or_create({'address': address})[0]
            self.cc.connect(email_address)

        for address in get_bcc(lines):
            if not address:
                continue
            email_address = EmailAddress.get_or_create({'address': address})[0]
            self.bcc.connect(email_address)

        from_email_address = get_from(lines)
        if from_email_address:
            email_address = EmailAddress.get_or_create({'address': from_email_address})[0]
            self.from_address.connect(email_address)

        self.save()


def get_date(lines):
    for i in range(0, len(lines)):
        line = lines[i]
        if line.startswith("Date:"):
            date = datetime.strptime(line[5:].strip(' \n')[:-6],
                                     "%a, %d %b %Y %H:%M:%S %z")  # Fri, 1 Dec 2000 01:12:00 -0800 (PST)
            if date.year < 1000:
                date = date.replace(year=date.year + 2000)
            return date
    return None


def get_from(lines):
    for i in range(0, len(lines)):
        line = lines[i]
        if line.startswith("From:"):
            return line[5:].strip(' \n')
    return None


# TODO: Only include email address (according to RFC spec: https://en.wikipedia.org/wiki/Email_address#Syntax)
def get_addresses(lines, address_type):
    def to_email_list(address_str):
        return address_str.strip('\t \n,').replace(', ', ',').split(',')

    result = set()
    for i in range(0, len(lines)):
        line = lines[i]
        if line.startswith(address_type):
            result.update(to_email_list(line[len(address_type):]))
            while i < len(lines) - 1 and lines[i + 1].count(':') == 0:
                i += 1
                line = lines[i]
                result.update(to_email_list(line))
            break
    return list(result)


def get_to(lines):
    return get_addresses(lines, "To:")


def get_cc(lines):
    return get_addresses(lines, "Cc:")


def get_bcc(lines):
    return get_addresses(lines, "Bcc:")


def get_subject(lines):
    for i in range(0, len(lines)):
        line = lines[i]
        if line.startswith('Subject:'):
            return line[8:].strip(' \n')
    return ""


def get_body(lines):
    content = ''
    for i in range(0, len(lines)):
        line = lines[i]
        if line.startswith('X-FileName:'):
            i += 1
            while i < len(lines):
                line = lines[i]
                content += line + '\n'
                i += 1
            break
    return content
