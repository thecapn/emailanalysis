import neomodel as model


class Folder(model.StructuredNode):
    name = model.StringProperty(required=True)

    belongs_to = model.RelationshipTo('.Person.Person', 'BELONGSTO')
    emails = model.RelationshipFrom('.Email.Email', 'IN')
