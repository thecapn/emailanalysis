import neomodel as model


class EmailAddress(model.StructuredNode):
    address = model.StringProperty(required=True, unique_index=True)

    to = model.RelationshipFrom('.Email.Email', 'TO')
    cc = model.RelationshipFrom('.Email.Email', 'CC')
    bcc = model.RelationshipFrom('.Email.Email', 'BCC')
    from_address = model.RelationshipFrom('.Email.Email', 'FROM')

    def __str__(self):
        return str(self.address)
