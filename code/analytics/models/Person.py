import neomodel as model


class Person(model.StructuredNode):
    name = model.StringProperty(required=True, index=True)

    folders = model.RelationshipFrom('.Folder.Folder', 'BELONGSTO')
