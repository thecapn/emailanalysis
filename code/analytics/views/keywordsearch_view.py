from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage

from analytics.models.Email import Email
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from analytics.views.forms.keword_search_form import KeywordSearchForm
from analytics.views.charts.LineChartJSONView import LineChartJSONView


@login_required
def key_word_search(request):
    form = KeywordSearchForm(request.GET)

    results_list = []
    if form.is_valid():
        keywords = form.cleaned_data['keywords']
        start_date = form.cleaned_data['start_date']
        end_date = form.cleaned_data['end_date']
        people = form.cleaned_data['people']

        results = Email.search(keywords, start_date, end_date, people)

        graph_counts = [0] * 24
        for res in results:
            graph_counts[res.date_cst.hour] += 1

        for item in results:
            results_list.append({
                'id': item._id,
                'subject': item.subject,
                'date': item.date_cst,
                'body': item.body,
                'from_address': [address.address for address in item.from_address],
                'to': [address.address for address in item.to],
                'cc': [address.address for address in item.cc],
                'bcc': [address.address for address in item.bcc]
            })

            paginator = Paginator(results_list, 50)
            page = request.GET.get('page')
            try:
                results = paginator.page(page)
            except PageNotAnInteger:
                results = paginator.page(1)
            except EmptyPage:
                results = paginator.page(paginator.num_pages)

        return render(request, 'keywordsearch.html',
                      {'found': True, 'keywords': keywords, 'start_date': start_date or "",
                       'end_date': end_date or "", 'people': people, 'results': results,
                       'form': form, 'max_count_array_value': max(graph_counts), 'graph_counts': graph_counts})

    form = KeywordSearchForm()
    return render(request, 'keywordsearch.html',
                  {'found': False, 'keywords': "", 'start_date': "", 'end_date': "",
                   'people': "", 'results': results_list, 'form': form})


@login_required
def make_graph(request):
    counts = request.GET.get('counts', '0,' * 24).strip(',')
    email_array = list(map(int, counts.split(',')))

    LineChartJSONView.set_data(email_array)
    return LineChartJSONView.as_view()(request)
