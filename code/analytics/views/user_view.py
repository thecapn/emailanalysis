from analytics.models.EmailAddress import EmailAddress
import random
from django.shortcuts import render


def load_user(request, email_address):
    address_obj = EmailAddress.nodes.get(address=email_address)
    all_emails = (address_obj.to, address_obj.cc, address_obj.bcc, address_obj.from_address)

    email_address_nodes = {}
    edges = {}

    email_address_nodes[email_address] = {
        'id': email_address,
        'x': random.randint(-20, 20),
        'y': random.randint(-20, 20),
        'label': email_address,
        'size': 0,
        'color': '#FFD600'
    }
    get_nodes_and_edges(all_emails, email_address_nodes, edges)
    return render(request, 'user.html', {'nodes': email_address_nodes, 'edges': edges, 'email_address': email_address})


def get_nodes_and_edges(all_emails, email_address_nodes, edges):
    for email_type in all_emails:
        for email in email_type:
            from_ = email.from_address[0][0]
            insert_or_update_address_node(from_, email_address_nodes)
            for address_type in (email.to, email.cc, email.bcc):
                for address in address_type:
                    insert_or_update_address_node(address, email_address_nodes)
                    insert_or_update_edge(from_, address, edges)
    return email_address_nodes, edges


def insert_or_update_edge(from_, to, all_edges):
    edge_id = (from_.address + to.address)
    edge = all_edges.get(edge_id, {
        'id': edge_id,
        'source': from_.address,
        'target': to.address,
        'size': 0
    })
    edge['size'] += 1
    all_edges[edge_id] = edge


def insert_or_update_address_node(address, all_nodes):
    address_str = address.address
    address_node = all_nodes.get(address_str, {
        'id': address_str,
        'x': random.randint(-20, 20),
        'y': random.randint(-20, 20),
        'label': address_str,
        'size': 0
    })
    address_node['size'] += 1
    all_nodes[address_str] = address_node


def connection(request, from_address, to_address):
    from_obj = EmailAddress.nodes.get(address=from_address)
    to_obj = EmailAddress.nodes.get(address=to_address)

    all_emails = [from_obj.to, from_obj.cc, from_obj.bcc, from_obj.from_address,
                  to_obj.to, to_obj.cc, to_obj.bcc, to_obj.from_address]

    email_address_nodes = {}
    edges = {}

    get_nodes_and_edges(all_emails, email_address_nodes, edges)
    return render(request, 'user.html', {
        'nodes': email_address_nodes,
        'edges': edges,
        "email_address": '{} -> {}'.format(from_address, to_address)})