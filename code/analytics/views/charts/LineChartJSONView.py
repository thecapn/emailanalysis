from chartjs.views.lines import BaseLineChartView

class LineChartJSONView(BaseLineChartView):
    email_data = []

    def get_labels(self):
        return ["12 AM"] + ["{} AM".format(i) for i in range(1, 12)] + \
               ["12 PM"] + ["{} PM".format(i) for i in range(1, 12)]

    def get_data(self):
        return [self.email_data]

    def get_datasets(self):
        datasets = []
        for i, data in enumerate(self.get_data()):
            dataset = {
                       'label': "Emails sent by hour",
                       'fillColor': "rgba(110, 70, 100, .5)",
                       'strokeColor': "rgba(0, 180, 200, .9)",
                       'pointColor': "rgba(0, 0, 255, .2)",
                       'pointStrokeColor': '#c6e2ff',
                       'pointHighlightFill': "#fff",
                       'pointHighlightStroke': "rgba(220,220,220,1)",
                       'data': data}
            datasets.append(dataset)
        return datasets

    def get_colors(self):
        colors = ((110, 70, 100), (0, 180, 200), (0, 0, 255))
        while True:
            for c in colors:
                yield c

    @classmethod
    def set_data(cls, data):
        cls.email_data = data
