from neomodel import db

from analytics.cypher import combine_subqueries, escape_cypher
from analytics.models.EmailAddress import EmailAddress
from django.contrib.auth.decorators import login_required
from django.shortcuts import render


@login_required
def search_address(request):
    success = False
    addresses = []
    value = request.GET.get("address", "")
    if value:
        success = True
        query = combine_subqueries(EmailAddress, "n.address =~ '(?is).*{}.*'".format(escape_cypher(value)))
        results, meta = db.cypher_query(query)
        addresses = [EmailAddress.inflate(row[0]) for row in results]
    return render(request, 'search.html', {'value': value, 'success': success, 'results': addresses})

