import errno

import os
import shutil
import tempfile
import threading
import zipfile
from _md5 import md5

from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from analytics.models.Email import Email, get_body
from analytics.models.Folder import Folder
from analytics.models.Person import Person
from analytics.views.forms.upload_form import UploadForm


@login_required
def upload_data(request):
    success = False
    if request.method == "POST":
        form = UploadForm(request.POST, request.FILES)
        if form.is_valid():
            _, zipf_path = tempfile.mkstemp(suffix="s.zip")
            zip_file = open(zipf_path, mode='wb')
            zip_file.write(request.FILES['emails_zipped'].read())

            lock = threading.Lock()
            lock.acquire()
            threading.Thread(target=process_people, args=(zipf_path, lock)).start()
            lock.acquire()

            try:
                os.remove(zipf_path)
            except PermissionError:
                pass

            success = True
    else:
        form = UploadForm()
    return render(request, 'upload.html', {'form': form, 'success': success})


def process_people(zip_path, lock):
    directory_path_orig = tempfile.mkdtemp()
    directory_path = directory_path_orig + "/"
    zip_file = zipfile.ZipFile(zip_path)
    zip_file.extractall(directory_path)
    lock.release()

    for directory in os.listdir(directory_path):
        if directory.startswith('.') or directory.startswith('_'):
            # https://bitbucket.org/ned/coveragepy/issues/198/continue-marked-as-not-covered
            continue  # pragma: no cover
        person = Person(name=directory)
        person.save()

        child_dir = directory_path + directory + '/'
        if os.path.isdir(child_dir):
            extract_folders(child_dir, person)

    try:
        shutil.rmtree(directory_path_orig)
    except OSError as exc:
        if exc.errno != errno.ENOENT:
            raise


def extract_folders(directory_path, person):
    folders = []
    for directory in os.listdir(directory_path):
        folder = Folder(name=directory)
        folder.save()

        folder.belongs_to.connect(person)
        folder.save()

        folders.append(folder)
        child_dir = directory_path + directory + '/'
        if os.path.isdir(child_dir):
            extract_emails(child_dir, folder)
    return folders


def extract_emails(directory_path, folder_object):
    new_emails = []
    for file_name in os.listdir(directory_path):
        if file_name.startswith('.') or os.path.isdir(directory_path + file_name):
            continue
        with open(directory_path + file_name, "r", encoding='utf-8', errors='ignore') as file_obj:
            file_lines = file_obj.readlines()
            new_emails.append(get_or_create_email(file_lines, folder_object))
    return new_emails


def get_or_create_email(file_lines, folder_object):
    body = get_body(file_lines)
    try:
        existing_email = Email.nodes.get(body_hash=Email.get_body_hash(body))
        if existing_email.body != body:
            raise Email.DoesNotExist("Hash Collision, (Hashes match, but bodies are different)")
    except Email.DoesNotExist:
        new_email = Email()
        new_email.fill_data_from_string(folder_object, file_lines)
        return new_email
    else:
        existing_email.in_folder.connect(folder_object)
        existing_email.save()
        return existing_email
