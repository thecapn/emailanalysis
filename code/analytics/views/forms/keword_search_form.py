import operator

from django import forms


class CustomDateInput(forms.TextInput):
    input_type = 'date'


class KeywordSearchForm(forms.Form):
    keywords = forms.CharField(required=False)
    start_date = forms.DateField(required=False, widget=CustomDateInput())
    end_date = forms.DateField(required=False, widget=CustomDateInput())
    people = forms.CharField(required=False)

    def clean(self):
        if any(self.cleaned_data.values()):
            return self.cleaned_data
        else:
            raise forms.ValidationError('')
