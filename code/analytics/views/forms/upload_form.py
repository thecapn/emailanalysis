from django import forms


class UploadForm(forms.Form):
    emails_zipped = forms.FileField()
