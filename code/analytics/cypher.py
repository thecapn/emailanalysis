from datetime import datetime, time


def escape_cypher(string):
    return string.replace("\\", "\\\\").replace("'", r"\'").replace('"', r'\"')
    # Not using raw strings on the first replace because python still considers the backslash to escape the final
    # quotation if it comes directly before it. Therefore the strings look exactly the same whether entered as raw
    # strings or not. See: http://stackoverflow.com/a/2081708/1608327


def get_keyword_subquery(keywords):
    if not keywords:
        return ""
    subquery_format = "(n.subject_in_db =~ '(?is).*{}.*' or n.body =~ '(?is).*{}.*')"

    keyword_list = keywords.split(' ')
    subquery = "("
    for kw in keyword_list:
        subquery += escape_cypher(kw) + "|"
    subquery = subquery[:-1] + ")"

    return subquery_format.format(subquery, subquery)


def get_email_address_subquery(addresses):
    if not addresses:
        return ""

    address_subqueries = []
    address_list = addresses.split(' ')
    for email_address in address_list:
        address_subqueries.append("({{address: '{}'}})-[]-(n)".format(escape_cypher(email_address)))
    return ' and '.join(address_subqueries)


def get_start_date_subquery(start_date):
    if not start_date:
        return ""

    start_date = datetime.combine(start_date, time(hour=0, minute=0, second=0))
    return "n.date > {}".format(start_date.timestamp())


def get_end_date_subquery(end_date):
    if not end_date:
        return ""

    end_date = datetime.combine(end_date, time(hour=23, minute=59, second=59))
    return "n.date < {}".format(end_date.timestamp())


def combine_subqueries(model, *subqueries):
    subqueries = list(filter(lambda x: x, subqueries))
    if len(subqueries) == 0:
        return "match(n:{}) return n;".format(model.__name__)

    query_begin = "match(n:{}) where ".format(model.__name__)
    query_middle = ' and '.join(subqueries)
    query_end = " return n;"
    return query_begin + query_middle + query_end
