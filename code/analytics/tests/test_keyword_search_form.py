from django.test import SimpleTestCase
from analytics.views.forms.keword_search_form import KeywordSearchForm


class KeywordSearchFormTests(SimpleTestCase):
    def test_no_filled_forms_results_in_invalid_form(self):
        form = KeywordSearchForm({'keywords': '', 'start_date': '', 'end_date': '', 'people': ''})
        self.assertFalse(form.is_valid())

    def test_one_filled_form_results_in_success(self):
        form = KeywordSearchForm({'keywords': 'keywords', 'start_date': '', 'end_date': '', 'people': ''})
        self.assertTrue(form.is_valid())
        self.assertEqual({'keywords': 'keywords', 'start_date': None, 'end_date': None, 'people': ''},
                         form.cleaned_data)
