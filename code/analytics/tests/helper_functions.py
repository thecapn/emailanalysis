import random
import string

from django.contrib.auth.models import User


def create_user(username=None):
    if username is None:
        username = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))
    user = User.objects.create_user(username=username, password='testpass', email=username + '@test.org')
    user.save()
    return user


def delete_email(email):
    for addr in email.to:
        addr.delete()
    for addr in email.cc:
        addr.delete()
    for addr in email.bcc:
        addr.delete()
    for addr in email.from_address:
        addr.delete()
    email.delete()


def delete_folder(folder):
    for email in folder.emails:
        delete_email(email)
    folder.delete()


def raise_fake_no_such_file(path):
    open("analytics/tests/test_data/fictional/directory")


def raise_other_os_error(path):
    raise OSError("Other")


def release_and_delete(zip_path, lock):
    lock.release()
    return
