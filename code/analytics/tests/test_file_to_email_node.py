from analytics.models.Email import get_cc, get_bcc, get_to, Email, get_subject, get_body, get_from
from django.test import TestCase


class CcBccToTests(TestCase):
    def test_single_line_cc_is_read(self):
        email = ["Cc: xxx@gmail.com, yyy@gmail.com, zzz@gmail.com"]
        self.assertEqual(set(get_cc(email)), {"xxx@gmail.com", "yyy@gmail.com", "zzz@gmail.com"})

    def test_single_line_bcc_is_read(self):
        email = ["Bcc: xxx@gmail.com, yyy@gmail.com, zzz@gmail.com"]
        self.assertEqual(set(get_bcc(email)), {"xxx@gmail.com", "yyy@gmail.com", "zzz@gmail.com"})

    def test_single_line_to_is_read(self):
        email = ["To: xxx@gmail.com, yyy@gmail.com, zzz@gmail.com"]
        self.assertEqual(set(get_to(email)), {"xxx@gmail.com", "yyy@gmail.com", "zzz@gmail.com"})

    def test_all_lines_of_multi_line_cc_are_read(self):
        email = ["Cc: xxx@gmail.com, yyy@gmail.com,", "zzz@gmail.com", "Other: blah blah blah"]
        self.assertEqual(set(get_cc(email)), {"xxx@gmail.com", "yyy@gmail.com", "zzz@gmail.com"})

    def test_all_lines_of_multi_line_bcc_are_read(self):
        email = ["Bcc: xxx@gmail.com, yyy@gmail.com,", "zzz@gmail.com", "Other: blah blah blah"]
        self.assertEqual(set(get_bcc(email)), {"xxx@gmail.com", "yyy@gmail.com", "zzz@gmail.com"})

    def test_all_lines_of_multi_line_to_are_read(self):
        email = ["To: xxx@gmail.com, yyy@gmail.com,", "zzz@gmail.com", "Other: blah blah blah"]
        self.assertEqual(set(get_to(email)), {"xxx@gmail.com", "yyy@gmail.com", "zzz@gmail.com"})

    def test_email_without_tos_returns_empty_list(self):
        email = ["Bcc: xxx@gmail.com", "Other: blah blah blah"]
        self.assertEqual(len(get_to(email)), 0)

    def test_email_without_ccs_returns_empty_list(self):
        email = ["To: xxx@gmail.com", "Other: blah blah blah"]
        self.assertEqual(len(get_cc(email)), 0)

    def test_email_without_bccs_returns_empty_list(self):
        email = ["Cc: xxx@gmail.com", "Other: blah blah blah"]
        self.assertEqual(len(get_bcc(email)), 0)


class SubjectTest(TestCase):
    def test_email_with_subject_retrieves_it(self):
        email = ["Subject: Hello", "Other: Nonsense"]
        self.assertEqual(get_subject(email), "Hello")

    def test_subject_with_extra_whitespace_trimmed(self):
        email = ["Subject: Hello     ", "Other: Nonsense"]
        self.assertEqual(get_subject(email), "Hello")

    def test_email_without_subject_returns_empty_string(self):
        email = ["Other: Nonsense"]
        self.assertEqual(get_subject(email), "")


class BodyTest(TestCase):
    def test_single_line_body_is_read(self):
        email = ["X-FileName:", "blah blah blah"]
        self.assertEqual(get_body(email), "blah blah blah\n")

    def test_multiline_body_separated_by_newlines(self):
        email = ["X-FileName:", "blah blah blah", "bluh bluh"]
        self.assertEqual(get_body(email), "blah blah blah\nbluh bluh\n")

    def test_contentless_email_has_empty_body(self):
        email = ["X-FileName:"]
        self.assertEqual(get_body(email), "")


class FromTest(TestCase):
    def test_from_is_extracted(self):
        email = ["From: xxx@zzz.org"]
        self.assertEqual(get_from(email), "xxx@zzz.org")

    def test_email_with_no_from_label_has_no_from(self):
        email = ["Other: blah blah blah"]
        self.assertEqual(get_from(email), None)

