from django.test import TestCase

from analytics.tests.helper_functions import create_user


class SearchEmailTests(TestCase):
    def test_url_uses_correct_template(self):
        user = create_user()
        self.client.login(username=user.username, password="testpass")

        result = self.client.get("/search/email/")

        self.assertIn("search.html", [t.name for t in result.templates])

    def test_simple_query_doesnt_crash(self):
        user = create_user()
        self.client.login(username=user.username, password="testpass")

        result = self.client.get("/search/email/", {"address": "test@test.com"})
        self.assertIn("search.html", [t.name for t in result.templates])
