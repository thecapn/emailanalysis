from datetime import datetime
from unittest.mock import patch

from django.test import TestCase

from analytics.models.Email import Email
from analytics.models.EmailAddress import EmailAddress
from analytics.tests.helper_functions import delete_email
from analytics.views.user_view import insert_or_update_address_node, insert_or_update_edge, get_nodes_and_edges


class UserViewTest(TestCase):

    def setUp(self):
        self.address_a = EmailAddress(address="address@domain.com")
        self.address_a.save()
        self.address_b = EmailAddress(address="addressb@domain.com")
        self.address_b.save()

    def tearDown(self):
        self.address_a.delete()
        self.address_b.delete()

    def test_user_url_resolves(self):
        response = self.client.get('/user/address@domain.com')
        self.assertEqual(response.status_code, 200)

    def test_connection_url_resolves(self):
        response = self.client.get('/connection/address@domain.com/addressb@domain.com')
        self.assertEqual(response.status_code, 200)


class AddNode(TestCase):
    def setUp(self):
        self.address_a = EmailAddress(address="address2@domain.com")
        self.address_a.save()

    def tearDown(self):
        self.address_a.delete()

    def test_add_graph_node(self):
        nodes = {}

        insert_or_update_address_node(self.address_a, nodes)
        self.assertTrue(1 == 1)


class AddEdge(TestCase):
    def setUp(self):
        self.address_a = EmailAddress(address="address3@domain.com")
        self.address_b = EmailAddress(address="address4@domain.com")
        self.address_a.save()
        self.address_b.save()

    def tearDown(self):
        self.address_a.delete()
        self.address_b.delete()

    def test_add_edge(self):
        blank_dict = {}
        insert_or_update_edge(self.address_a, self.address_b, blank_dict)
        self.assertTrue(blank_dict)


class GetNodesAndEdges(TestCase):
    def setUp(self):
        with patch('neomodel.properties.logger'):  # Silence timezone warning
            self.emails = [
                Email(date=datetime(year=2002, month=2, day=15, hour=8, minute=30, second=0),
                      subject_in_db="RE: keyword", body="Some more text"),
            ]
            for email in self.emails:
                email.save()

        self.addresses = [
            EmailAddress(address="guy@domain.org"),
            EmailAddress(address="friend@domain.org"),
            EmailAddress(address="pal@domain.org"),
            EmailAddress(address="buddy@domain.org"),
        ]
        for address in self.addresses:
            address.save()

        self.emails[0].to.connect(self.addresses[0])
        self.emails[0].cc.connect(self.addresses[1])
        self.emails[0].bcc.connect(self.addresses[2])
        self.emails[0].from_address.connect(self.addresses[3])
        self.emails[0].save()

    def tearDown(self):
        for email in self.emails:
            delete_email(email)
        for address in self.addresses:
            address.delete()

    def test_get_nodes_and_edges(self):
        nodes, edges = get_nodes_and_edges(([self.emails[0]], [], [], []), {}, {})
        self.assertIn("buddy@domain.org", nodes)

    def test_receiver_nodes(self):
        nodes, edges = get_nodes_and_edges(([self.emails[0]], [], [], []), {}, {})
        self.assertIn("friend@domain.org", nodes)
        self.assertIn("pal@domain.org", nodes)
        self.assertIn("guy@domain.org", nodes)

    def test_edges(self):
        nodes, edges = get_nodes_and_edges(([self.emails[0]], [], [], []), {}, {})
        self.assertIn("buddy@domain.org" + "friend@domain.org", edges)
        self.assertIn("buddy@domain.org" + "pal@domain.org", edges)
        self.assertIn("buddy@domain.org" + "guy@domain.org", edges)




