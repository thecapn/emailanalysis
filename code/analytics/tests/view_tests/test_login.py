from django.core.urlresolvers import reverse
from django.test import TestCase

from analytics.tests.helper_functions import create_user
from deep_email.settings import LOGIN_REDIRECT_URL


class SearchKeywordTests(TestCase):
    def test_url_uses_correct_template(self):
        result = self.client.get("/")

        self.assertIn("login.html", [t.name for t in result.templates])

    def test_login_succeeds(self):
        user = create_user()

        result = self.client.post("/", {"username": user.username, "password": "testpass"})

        self.assertRedirects(result, reverse(LOGIN_REDIRECT_URL))

    def test_bad_login_fails(self):
        user = create_user()

        result = self.client.post("/", {"username": user.username, "password": "testpassWRONG"})

        self.assertIn("login.html", [t.name for t in result.templates])

    def test_login_with_redirect_succeeds(self):
        user = create_user()

        result = self.client.post("/?next=/upload", {"username": user.username, "password": "testpass"})

        self.assertRedirects(result, "/upload")
