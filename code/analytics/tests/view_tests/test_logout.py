from django.test import TestCase

from analytics.tests.helper_functions import create_user


class SearchKeywordTests(TestCase):
    def test_url_uses_correct_template(self):
        user = create_user()
        self.client.login(username=user.username, password="testpass")

        result = self.client.get("/logout/")

        self.assertRedirects(result, "/")
