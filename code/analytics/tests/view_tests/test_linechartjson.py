from django.test import TestCase

from analytics.views.charts.LineChartJSONView import LineChartJSONView


class LineChartTests(TestCase):

    def test_linechart_labels_correct(self):
        mock_line_chart = LineChartJSONView()
        correct_array = ["12 AM"] + ["{} AM".format(i) for i in range(1, 12)] + \
                        ["12 PM"] + ["{} PM".format(i) for i in range(1, 12)]
        self.assertEqual(correct_array, LineChartJSONView.get_labels(mock_line_chart))

    def test_data_after_setting(self):
        mock_line_chart = LineChartJSONView()
        preset_array = [13, 2, 4, 3, 2, 1, 5, 8, 10]
        mock_line_chart.set_data(preset_array)
        self.assertEqual([preset_array], LineChartJSONView.get_data(mock_line_chart))

    def test_dataset_is_correct(self):
        mock_line_chart = LineChartJSONView()
        mock_line_chart.set_data([])
        correct_dataset = {
            'label': "Emails sent by hour",
            'fillColor': "rgba(110, 70, 100, .5)",
            'strokeColor': "rgba(0, 180, 200, .9)",
            'pointColor': "rgba(0, 0, 255, .2)",
            'pointStrokeColor': '#c6e2ff',
            'pointHighlightFill': "#fff",
            'pointHighlightStroke': "rgba(220,220,220,1)",
            'data': []}
        self.assertEqual([correct_dataset], LineChartJSONView.get_datasets(mock_line_chart))

    def test_colors(self):
        mock_line_chart = LineChartJSONView()
        color = next(LineChartJSONView.get_colors(mock_line_chart))
        self.assertIsInstance(color, tuple)