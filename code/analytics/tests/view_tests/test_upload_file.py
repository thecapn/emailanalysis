import os
import threading
import zipfile

from datetime import datetime
from unittest.mock import patch

from py2neo import Graph

from analytics.models.Email import Email
from django.test import TestCase

from analytics.models.Person import Person
from analytics.views.upload_dataset import extract_folders, extract_emails, get_or_create_email, process_people
from analytics.models.Folder import Folder
from analytics.tests.helper_functions import create_user, delete_email, delete_folder, raise_fake_no_such_file, \
    raise_other_os_error, release_and_delete
from deep_email import settings
from analytics.views.forms.upload_form import UploadForm


class UploadFileViewTest(TestCase):
    def test_url_uses_correct_template(self):
        user = create_user()
        self.client.login(username=user.username, password="testpass")

        result = self.client.get("/upload/")

        self.assertIn("upload.html", [t.name for t in result.templates])


class UploadFileEmailTest(TestCase):
    def setUp(self):
        with patch('neomodel.properties.logger'):  # Silence timezone warning
            self.email = Email(date=datetime(year=2002, month=2, day=15, hour=8, minute=30, second=0),
                               subject_in_db="SUBJECT", body="BODY\n")
            self.email.save()

        self.folder = Folder(name="folder")
        self.folder.save()

    def tearDown(self):
        self.folder.delete()
        self.email.delete()

    def test_existing_email_is_connected_to_folder(self):
        with patch('neomodel.properties.logger'):  # Silence timezone warning
            updated_email = get_or_create_email(["Subject: SUBJECT", "X-FileName:", "BODY"], self.folder)

        self.assertIn(self.folder, updated_email.in_folder)
        delete_email(updated_email)

    @patch('analytics.models.Email.Email.get_body_hash')
    def test_hash_collision_is_recognized_as_new_email(self, md5_mock):
        md5_mock.return_value = self.email.body_hash

        with patch('neomodel.properties.logger'):  # Silence timezone warning
            new_email = get_or_create_email(["Date: Mon, 14 May 2001 19:36:00 -0700 (PDT)",
                                             "Subject: SUBJECT", "X-FileName:", "BODYX"], self.folder)

        self.assertNotEqual(self.email, new_email)
        delete_email(new_email)

    def test_new_email_is_created(self):
        with patch('neomodel.properties.logger'):  # Silence timezone warning
            new_email = get_or_create_email(["Date: Mon, 14 May 2001 19:36:00 -0700 (PDT)",
                                             "Subject: SUBJECT", "X-FileName:", "BODYX"], self.folder)

        self.assertNotEqual(self.email, new_email)
        delete_email(new_email)

    def test_dir_with_2_emails_produces_2_email_objects(self):
        emails = extract_emails('analytics/tests/test_data/test1/inbox/', self.folder)
        self.assertEqual(2, len(emails))
        for email in emails:
            delete_email(email)

    def test_ignores_subdirectory(self):
        emails = extract_emails('analytics/tests/test_data/test_inbox_with_subdir/inbox/', self.folder)
        self.assertEqual(2, len(emails))
        for email in emails:
            delete_email(email)


class UploadFileFolderTest(TestCase):
    def setUp(self):
        self.person = Person(name="doe-j")
        self.person.save()

    def tearDown(self):
        self.person.delete()

    def test_directory_with_2_folders_finds_2_folders(self):
        folders = extract_folders('analytics/tests/test_data/test1/', self.person)
        self.assertEqual(2, len(folders))
        for f in folders:
            delete_folder(f)
            
            
class UploadFilePeopleTest(TestCase):
    def setUp(self):
        self.zip_path = './analytics/tests/test_data/testzip.zip'
        zip_obj = zipfile.ZipFile(self.zip_path, 'w')

        zip_obj.write('./analytics/tests/test_data/testzip/inbox/email.email', arcname="testzip/inbox/1.email")
        zip_obj.close()

        self.lock = threading.Lock()
        self.lock.acquire()

    def tearDown(self):
        os.remove(self.zip_path)

    @patch('analytics.views.upload_dataset.extract_folders')
    @patch('analytics.views.upload_dataset.Person')
    def test_single_directory_creates_single_person(self, person_mock, extract_mock):
        process_people(self.zip_path, self.lock)
        person_mock.assert_called_once_with(name="testzip")

    @patch('analytics.views.upload_dataset.extract_folders')
    @patch('analytics.views.upload_dataset.Person')
    def test_single_person_with_single_directory_has_it_extracted(self, person_mock, extract_mock):
        process_people(self.zip_path, self.lock)
        self.assertEqual(1, extract_mock.call_count)

    @patch('analytics.views.upload_dataset.extract_folders')
    @patch('analytics.views.upload_dataset.Person')
    @patch('analytics.views.upload_dataset.shutil.rmtree', raise_fake_no_such_file)
    def test_already_deleted_tempdir_doesnt_cause_error(self, person_mock, extract_mock):
        process_people(self.zip_path, self.lock)

    @patch('analytics.views.upload_dataset.extract_folders')
    @patch('analytics.views.upload_dataset.Person')
    @patch('analytics.views.upload_dataset.shutil.rmtree', raise_other_os_error)
    def test_other_os_error_is_reraised(self, person_mock, extract_mock):
        with self.assertRaises(OSError):
            process_people(self.zip_path, self.lock)

    @patch('analytics.views.upload_dataset.extract_folders')
    @patch('analytics.views.upload_dataset.Person')
    def test_hidden_folders_are_ignored(self, person_mock, extract_mock):
        zip_obj = zipfile.ZipFile(self.zip_path, 'a')
        zip_obj.write('./analytics/tests/test_data/file.email', arcname=".hidden/2.email")
        zip_obj.close()

        process_people(self.zip_path, self.lock)
        self.assertEqual(1, extract_mock.call_count)

    @patch('analytics.views.upload_dataset.process_people', release_and_delete)
    def test_upload_data(self):
        user = create_user()
        self.client.login(username=user.username, password="testpass")
        with open(self.zip_path, 'rb') as zip_file:
            response = self.client.post('/upload/', {'emails_zipped': zip_file})
        self.assertEquals(response.status_code, 200)
