from datetime import datetime
from unittest.mock import patch

from django.test import TestCase

from analytics.models.Email import Email
from analytics.tests.helper_functions import create_user, delete_email


class SearchKeywordTests(TestCase):

    def test_url_uses_correct_template(self):
        user = create_user()
        self.client.login(username=user.username, password="testpass")

        result = self.client.get("/search/keyword/")

        self.assertIn("keywordsearch.html", [t.name for t in result.templates])

    def test_sample_query_doesnt_crash(self):
        user = create_user()
        self.client.login(username=user.username, password="testpass")

        result = self.client.get("/search/keyword/", {"keywords": "test"})

        self.assertIn("keywordsearch.html", [t.name for t in result.templates])

    def test_make_graph_resolves(self):
        user = create_user()
        self.client.login(username=user.username, password="testpass")
        response = self.client.get('/line_chart/json/', data={'counts': '1,' * 24})
        self.assertEqual(response.status_code, 200)

    @patch('analytics.views.keywordsearch_view.LineChartJSONView.set_data')
    def test_graph_has_data_set_from_query_string(self, set_data_mock):
        user = create_user()
        self.client.login(username=user.username, password="testpass")
        self.client.get('/line_chart/json/', data={'counts': '1,' * 24})
        set_data_mock.assert_called_once_with([1] * 24)

    @patch('analytics.views.keywordsearch_view.LineChartJSONView.set_data')
    def test_graph_has_all_0s_if_no_counts_sent(self, set_data_mock):
        user = create_user()
        self.client.login(username=user.username, password="testpass")
        self.client.get('/line_chart/json/')
        set_data_mock.assert_called_once_with([0] * 24)


class SearchTest(TestCase):
    def setUp(self):
        user = create_user()
        self.client.login(username=user.username, password="testpass")

        self.emails = []
        with patch('neomodel.properties.logger'):  # Silence timezone warning
            for i in range(5):
                email = Email(date=datetime(year=2002, month=2, day=4, hour=8, minute=i, second=0),
                              subject_in_db="RE: keyword " + str(i), body="Some more text" + str(i))
                email.save()
                self.emails.append(email)

    def tearDown(self):
        for email in self.emails:
            delete_email(email)

    def test_keyword_search_has_5_results(self):
        result = self.client.get("/search/keyword/", {"keywords": "keyword"})
        self.assertEqual(5, len(result.context['results']))

    def test_keyword_search_paginated(self):
        with patch('neomodel.properties.logger'):  # Silence timezone warning
            for i in range(50):
                email = Email(date=datetime(year=2002, month=2, day=4, hour=8, minute=i, second=0),
                              subject_in_db="RE: keyword " + str(i), body="Some more text" + str(i))
                email.save()
                self.emails.append(email)
        result = self.client.get("/search/keyword/", {"keywords": "keyword"})
        self.assertEqual(50, len(result.context['results']))

    def test_keyword_search_empty_page_goes_to_last_page(self):
        result = self.client.get("/search/keyword/", {"keywords": "keyword", "page": '2'})
        self.assertEqual(5, len(result.context['results']))



