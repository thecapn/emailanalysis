from datetime import date, datetime

from django.test import SimpleTestCase
from analytics.cypher import get_keyword_subquery, get_email_address_subquery, get_end_date_subquery, \
    get_start_date_subquery, combine_subqueries, escape_cypher
from analytics.models.Email import Email


class QueryBuilderTest(SimpleTestCase):
    def test_no_keywords_result_in_empty_subquery(self):
        self.assertEqual("", get_keyword_subquery(""))

    def test_single_keyword_generates_regex_subquery(self):
        expected = "(n.subject_in_db =~ '(?is).*(keyword).*' or n.body =~ '(?is).*(keyword).*')"
        self.assertEqual(expected, get_keyword_subquery("keyword"))

    def test_two_keywords_generates_ored_regex_subquery(self):
        expected = "(n.subject_in_db =~ '(?is).*(keywordA|keywordB).*' or n.body =~ '(?is).*(keywordA|keywordB).*')"
        self.assertEqual(expected, get_keyword_subquery("keywordA keywordB"))

    def test_no_address_results_in_no_subquery(self):
        self.assertEqual("", get_email_address_subquery(""))

    def test_one_address_results_in_single_address_subquery(self):
        expected = "({address: 'person@domain.org'})-[]-(n)"
        self.assertEqual(expected, get_email_address_subquery("person@domain.org"))

    def test_two_address_results_in_anded_address_subquery(self):
        expected = "({address: 'person@domain.org'})-[]-(n) and ({address: 'otherperson@domain.org'})-[]-(n)"
        self.assertEqual(expected, get_email_address_subquery("person@domain.org otherperson@domain.org"))

    def test_no_start_date_results_in_no_start_date_subquery(self):
        self.assertEqual("", get_start_date_subquery(None))

    def test_start_date_generates_time_stamp_from_beginning_of_day(self):
        expected = "n.date > {}".format(datetime(year=2002, month=2, day=15, hour=0, minute=0, second=0).timestamp())
        self.assertEqual(expected, get_start_date_subquery(date(year=2002, month=2, day=15)))

    def test_no_end_date_results_in_no_end_date_subquery(self):
        self.assertEqual("", get_end_date_subquery(None))

    def test_start_date_generates_time_stamp_from_end_of_day(self):
        expected = "n.date < {}".format(datetime(year=2002, month=2, day=15, hour=23, minute=59, second=59).timestamp())
        self.assertEqual(expected, get_end_date_subquery(date(year=2002, month=2, day=15)))

    def test_no_subqueries_returns_query_for_all(self):
        expected = "match(n:Email) return n;"
        self.assertEqual(expected, combine_subqueries(Email))

    def test_empty_subqueries_ignored(self):
        expected = "match(n:Email) where foo return n;"
        self.assertEqual(expected, combine_subqueries(Email, "", "foo"))

    def test_multiple_subqueries_anded(self):
        expected = "match(n:Email) where foo and bar return n;"
        self.assertEqual(expected, combine_subqueries(Email, "foo", "bar"))


class CypherEscapeTests(SimpleTestCase):
    def test_string_containing_no_special_chars_is_not_escaped(self):
        input_string = "qwertyuiopasdfghjklzxcvbnm1234567890!@#$%^&*()"
        self.assertEqual(input_string, escape_cypher(input_string))

    def test_string_containing_single_quote_is_escaped(self):
        input_string = r"qwertyuiopasdfghjklzxcvbnm12345'67890!@#$%^&*()"
        expected_string = r"qwertyuiopasdfghjklzxcvbnm12345\'67890!@#$%^&*()"
        self.assertEqual(expected_string, escape_cypher(input_string))

    def test_string_containing_double_quote_is_escaped(self):
        input_string = r'qwertyuiopasdfghjklzxcv"bnm1234567890!@#$%^&*()'
        expected_string = r'qwertyuiopasdfghjklzxcv\"bnm1234567890!@#$%^&*()'
        self.assertEqual(expected_string, escape_cypher(input_string))

    def test_string_containing_back_to_back_single_quotes_is_escaped(self):
        input_string = r"qwertyuiopasdfghjklzxcvbnm12345''67890!@#$%^&*()"
        expected_string = r"qwertyuiopasdfghjklzxcvbnm12345\'\'67890!@#$%^&*()"
        self.assertEqual(expected_string, escape_cypher(input_string))

    def test_string_containing_back_to_back_double_quotes_is_escaped(self):
        input_string = r'qwertyuiopasdfghjklzxcv""bnm1234567890!@#$%^&*()'
        expected_string = r'qwertyuiopasdfghjklzxcv\"\"bnm1234567890!@#$%^&*()'
        self.assertEqual(expected_string, escape_cypher(input_string))

    def test_string_containing_backslashes_is_escaped(self):
        input_string = r"qwertyuiopasdfghjklzxcv\bnm1234567890!@#$%^&*()"
        expected_string = r"qwertyuiopasdfghjklzxcv\\bnm1234567890!@#$%^&*()"
        self.assertEqual(expected_string, escape_cypher(input_string))

    def test_string_containing_all_three_is_escaped(self):
        input_string = r"qwertyuiopasdfghjklzxcv\bnm1234'56789\"0!@#$%^&*()"
        expected_string = r"qwertyuiopasdfghjklzxcv\\bnm1234\'56789\\\"0!@#$%^&*()"
        self.assertEqual(expected_string, escape_cypher(input_string))