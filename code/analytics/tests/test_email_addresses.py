from analytics.models.EmailAddress import EmailAddress
from django.test import TestCase


class EmailAddressTest(TestCase):
    def test_str_representation_of_email_address_is_the_address(self):
        address = EmailAddress()
        address.address = "test@test.org"
        self.assertEqual("test@test.org", str(address))
