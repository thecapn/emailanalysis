from unittest.mock import patch

import pytz

from analytics.models.Email import Email
from analytics.models.Email import get_date
from datetime import datetime, date

from analytics.models.EmailAddress import EmailAddress
from analytics.models.Folder import Folder
from django.test import TestCase


class EmailTest(TestCase):
    def test_date_cst_adjusts_timezone(self):
        email = Email(date=datetime(year=2002, month=6, day=15, hour=12, minute=0, second=0, tzinfo=pytz.utc))
        self.assertEqual(6, email.date_cst.hour)

    def test_presave_saves_a_hash_of_the_emails_body(self):
        email = Email()
        email.body = "blah blah blah"
        email.pre_save()
        self.assertEqual('55e562bfee2bde4f9e71b8885eb5e303', email.body_hash)

    def test_get_date_return_none(self):
        self.assertEqual(None, get_date([]))

    def test_get_date_return_date(self):
        line = "Date: Fri, 1 Dec 2000 01:12:00 -0800 (PST)"
        date = datetime.strptime(line[5:].strip(' \n')[:-6], "%a, %d %b %Y %H:%M:%S %z")
        self.assertEqual(date, get_date(["Message-ID: <32497489.1075861029532.JavaMail.evans@thyme>",
                                         "Date: Fri, 1 Dec 2000 01:12:00 -0800 (PST)"]))

    def test_get_date_return_date_input_year_under1000(self):
        line = "Date: Fri, 1 Dec 2009 01:12:00 -0800 (PST)"
        date = datetime.strptime(line[5:].strip(' \n')[:-6], "%a, %d %b %Y %H:%M:%S %z")
        self.assertEqual(date, get_date(["Date: Fri, 1 Dec 0009 01:12:00 -0800 (PST)"]))


class LoadEmailTest(TestCase):
    def setUp(self):
        self.folder = Folder(name="testfolder")
        self.folder.save()
        self.email = Email()

        self.addresses = [
            EmailAddress(address="management.ubsw@enron.com"),
            EmailAddress(address="robert.badeer@enron.com"),
            EmailAddress(address="bob@enron.com"),
            EmailAddress(address="steven@enron.com"),
        ]

        for address in self.addresses:
            address.save()

    def tearDown(self):
        self.folder.delete()
        self.email.delete()

        for address in self.addresses:
            address.delete()

    def test_fill_data_string_self_body(self):
        with open("./analytics/tests/test_data/testemail", "r", encoding='utf-8', errors='ignore') as file_obj:
            file_lines = file_obj.readlines()

        self.email.fill_data_from_string(self.folder, file_lines)
        self.assertEqual("EMailBody\n", self.email.body)

    def test_fill_data_string_self_subject(self):
        with open("./analytics/tests/test_data/testemail", "r", encoding='utf-8', errors='ignore') as file_obj:
            file_lines = file_obj.readlines()

        self.email.fill_data_from_string(self.folder, file_lines)
        self.assertEqual("EMailSubject", self.email.subject)

    def test_fill_data_string_self_date(self):
        with open("./analytics/tests/test_data/testemail", "r", encoding='utf-8', errors='ignore') as file_obj:
            file_lines = file_obj.readlines()

        dateline = "Date: Thu, 7 Mar 2002 14:45:49 -0800 (PST)"
        self.email.fill_data_from_string(self.folder, file_lines)
        self.assertEqual(datetime.strptime(dateline[5:].strip(' \n')[:-6], "%a, %d %b %Y %H:%M:%S %z"), self.email.date)

    def test_fill_data_string_self_folder(self):
        with open("./analytics/tests/test_data/testemail", "r", encoding='utf-8', errors='ignore') as file_obj:
            file_lines = file_obj.readlines()

        self.email.fill_data_from_string(self.folder, file_lines)
        email_folder = ''
        for inner_folder in self.email.in_folder:
            email_folder = inner_folder.name
        self.assertEqual(self.folder.name, email_folder)

    def test_fill_data_string_self_to(self):
        with open("./analytics/tests/test_data/testemail", "r", encoding='utf-8', errors='ignore') as file_obj:
            file_lines = file_obj.readlines()

        self.email.fill_data_from_string(self.folder, file_lines)
        email_to = ''
        for emailto in self.email.to:
            email_to = emailto.address
        self.assertEqual("robert.badeer@enron.com", email_to)

    def test_fill_data_string_self_from(self):
        with open("./analytics/tests/test_data/testemail", "r", encoding='utf-8', errors='ignore') as file_obj:
            file_lines = file_obj.readlines()

        self.email.fill_data_from_string(self.folder, file_lines)
        email_from = ''
        for emailfrom in self.email.from_address:
            email_from = emailfrom.address
        self.assertEqual("management.ubsw@enron.com", email_from)

    def test_fill_data_string_self_cc(self):
        with open("./analytics/tests/test_data/testemail", "r", encoding='utf-8', errors='ignore') as file_obj:
            file_lines = file_obj.readlines()

        self.email.fill_data_from_string(self.folder, file_lines)
        email_cc = []
        for emailcc in self.email.cc:
            email_cc.append(emailcc.address)
        self.assertEqual(["bob@enron.com"], email_cc)

    def test_fill_data_string_self_bcc(self):
        with open("./analytics/tests/test_data/testemail", "r", encoding='utf-8', errors='ignore') as file_obj:
            file_lines = file_obj.readlines()

        self.email.fill_data_from_string(self.folder, file_lines)
        email_bcc = []
        for emailbcc in self.email.bcc:
            email_bcc.append(emailbcc.address)
        self.assertEqual(["steven@enron.com"], email_bcc)

    def test_fill_data_string_self_to_noaddress(self):
        with open("./analytics/tests/test_data/testmail_noemailaddress", "r",
                  encoding='utf-8', errors='ignore') as file_obj:
            file_lines = file_obj.readlines()

        self.email.fill_data_from_string(self.folder, file_lines)
        email_to = []
        for emailto in self.email.to:
            email_to.append(emailto.address)
        self.assertEqual([], email_to)

    def test_fill_data_string_self_cc_noaddress(self):
        with open("./analytics/tests/test_data/testmail_noemailaddress", "r", encoding='utf-8',
                  errors='ignore') as file_obj:
            file_lines = file_obj.readlines()

        self.email.fill_data_from_string(self.folder, file_lines)
        email_cc = []
        for emailcc in self.email.cc:
            email_cc.append(emailcc.address)
        self.assertEqual([], email_cc)

    def test_fill_data_string_self_bcc_noaddress(self):
        with open("./analytics/tests/test_data/testmail_noemailaddress", "r", encoding='utf-8',
                  errors='ignore') as file_obj:
            file_lines = file_obj.readlines()
        self.email.fill_data_from_string(self.folder, file_lines)
        email_bcc = []
        for emailbcc in self.email.bcc:
            email_bcc.append(emailbcc.address)
        self.assertEqual([], email_bcc)


class EmailSearchTest(TestCase):
    def setUp(self):
        with patch('neomodel.properties.logger'):  # Silence timezone warning
            self.emails = [
                Email(date=datetime(year=2002, month=2, day=15, hour=8, minute=30, second=0),
                      subject_in_db="RE: keyword", body="Some more text"),
                Email(date=datetime(year=2002, month=2, day=15, hour=8, minute=30, second=0),
                      subject_in_db="FW: Keyword", body="Some keyWORD text"),
                Email(date=datetime(year=2002, month=5, day=15, hour=8, minute=30, second=0),
                      subject_in_db="Other Words", body="Some more keywords"),
                Email(date=datetime(year=2002, month=2, day=15, hour=8, minute=30, second=0),
                      subject_in_db="FW: Other Words", body="Some more other words")
            ]
            for email in self.emails:
                email.save()

            self.addresses = [
                EmailAddress(address="guy@domain.org"),
                EmailAddress(address="friend@domain.org"),
                EmailAddress(address="pal@domain.org")
            ]
            for address in self.addresses:
                address.save()

            self.emails[0].from_address.connect(self.addresses[0])
            self.emails[1].from_address.connect(self.addresses[0])

            self.emails[1].to.connect(self.addresses[1])

    def tearDown(self):
        for address in self.addresses:
            address.delete()
        for email in self.emails:
            email.delete()

    def test_search_gets_emails_containing_keyword_in_subject(self):
        search_results = Email.search('keyword', None, None, '')
        self.assertIn(self.emails[0], search_results)
        self.assertIn(self.emails[1], search_results)

    def test_search_gets_emails_containing_keyword_in_body(self):
        search_results = Email.search('keyword', None, None, '')
        self.assertIn(self.emails[1], search_results)
        self.assertIn(self.emails[2], search_results)

    def test_search_excluded_emails_without_keyword(self):
        search_results = Email.search('keyword', None, None, '')
        self.assertNotIn(self.emails[3], search_results)

    def test_search_gets_emails_after_start_date(self):
        search_results = Email.search('', date(year=2002, month=4, day=5), None, '')
        self.assertIn(self.emails[2], search_results)

    def test_search_does_not_get_emails_before_start_date(self):
        search_results = Email.search('', date(year=2002, month=4, day=5), None, '')
        self.assertNotIn(self.emails[0], search_results)
        self.assertNotIn(self.emails[1], search_results)
        self.assertNotIn(self.emails[3], search_results)

    def test_search_gets_emails_before_end_date(self):
        search_results = Email.search('', None, date(year=2002, month=4, day=5), '')
        self.assertIn(self.emails[0], search_results)
        self.assertIn(self.emails[1], search_results)
        self.assertIn(self.emails[3], search_results)

    def test_search_does_not_get_emails_after_end_date(self):
        search_results = Email.search('', None, date(year=2002, month=4, day=5), '')
        self.assertNotIn(self.emails[2], search_results)

    def test_search_includes_emails_with_one_address(self):
        search_results = Email.search('', None, None, 'guy@domain.org')
        self.assertIn(self.emails[0], search_results)
        self.assertIn(self.emails[1], search_results)

    def test_search_excludes_emails_without_one_address(self):
        search_results = Email.search('', None, None, 'guy@domain.org')
        self.assertNotIn(self.emails[2], search_results)
        self.assertNotIn(self.emails[3], search_results)

    def test_search_includes_emails_with_two_addresses(self):
        search_results = Email.search('', None, None, 'guy@domain.org friend@domain.org')
        self.assertIn(self.emails[1], search_results)

    def test_search_excludes_emails_without_two_addresses(self):
        search_results = Email.search('', None, None, 'guy@domain.org friend@domain.org')
        self.assertNotIn(self.emails[0], search_results)
        self.assertNotIn(self.emails[2], search_results)
        self.assertNotIn(self.emails[3], search_results)


class SubjectTests(TestCase):
    def test_email_with_no_subject_returns_correct_string(self):
        expected_return = Email.NO_SUBJECT_PLACEHOLDER
        email = Email()

        self.assertEqual(email.subject, expected_return)

    def test_email_with_subject_set_returns_subject(self):
        expected_return = "Test Subject"
        email = Email()
        email.subject = expected_return

        self.assertEqual(email.subject, expected_return)

    def test_setter_sets_correct_parameter(self):
        expected_return = "Test Subject"
        email = Email()
        email.subject = expected_return

        self.assertEqual(email.subject_in_db, expected_return)
