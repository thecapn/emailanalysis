from analytics.views import keywordsearch_view, login_view, search_view, upload_dataset, user_view
from django.conf.urls import url

urlpatterns = [
    url(r'^upload/?$', upload_dataset.upload_data, name="upload_data"),
    url(r'^search/email/?$', search_view.search_address, name="address_search"),
    url(r'^search/keyword/?$', keywordsearch_view.key_word_search, name="keyword_search"),
    url(r'^search/?$', search_view.search_address, name="search"),
    url(r'^logout/?$', login_view.user_logout, name='user_logout'),
    url(r'^$', login_view.user_login, name="user_login"),
    url(r'^user/(?P<email_address>.*)/?', user_view.load_user,
        name="user_detail"),
    url(r'^connection/(?P<from_address>.+)/(?P<to_address>.+)/?', user_view.connection,
        name="connection_detail"),
    url(r'^line_chart/json/$', keywordsearch_view.make_graph, name='line_chart_json')
]
